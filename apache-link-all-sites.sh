#!/usr/bin/env bash

ln -s /mnt/c/var/www/timesoms.com
ln -s /mnt/c/var/www/timespss.com
ln -s /mnt/c/var/www/timescbs.com
ln -s /mnt/c/var/www/timeswms.com
ln -s /mnt/c/var/www/timesrms.com
ln -s /mnt/c/var/www/v2.timeslogistics.info
ln -s /mnt/c/var/www/v2.times-reverse.com
ln -s /mnt/c/var/www/tec-oms.com
ln -s /mnt/c/var/www/timeseplatform.com