#!/usr/bin/env bash

git clone git@indzz.bitbucket.org:timesindzz/tec-times-oms-backend.git timesoms.com
git clone git@indzz.bitbucket.org:timesindzz/tec-pss-backend.git timespss.com
git clone git@indzz.bitbucket.org:timesindzz/tec-cbs-backend.git timescbs.com
git clone git@indzz.bitbucket.org:timesindzz/tec-wms-backend.git timeswms.com
git clone git@indzz.bitbucket.org:timesindzz/tec-rms-backend.git timesrms.com
git clone git@indzz.bitbucket.org:timesindzz/times-tms-backend.git v2.timeslogistics.info
git clone git@indzz.bitbucket.org:timesindzz/times-lazada-rms-backend.git v2.times-reverse.com
git clone git@indzz.bitbucket.org:timesindzz/tec-lazada-oms-backend.git tec-oms.com
git clone git@indzz.bitbucket.org:timesindzz/times-e-platform-backend.git timeseplatform.com